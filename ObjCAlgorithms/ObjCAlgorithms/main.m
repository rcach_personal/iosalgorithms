#import <Foundation/Foundation.h>

#import "RCBasicSymbolTableTestClient.h"
#import "RCFrequencyCounter.h"

int main(int argc, const char * argv[]) {
  @autoreleasepool {    
    [RCBasicSymbolTableTestClient test:[NSString stringWithUTF8String:argv[1]]];
  }
  return 0;
}

