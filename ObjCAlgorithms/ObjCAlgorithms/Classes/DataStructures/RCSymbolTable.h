#import <Foundation/Foundation.h>

@protocol RCSymbolTable <NSObject>

+ (instancetype)symbolTable;

- (void)putKey:(id<NSCopying, NSObject>)key value:(id)value;

- (id)getValueWithKey:(id<NSCopying, NSObject>)key;

- (void)deleteValueWithKey:(id<NSCopying, NSObject>)key;

- (BOOL)containsKey:(id<NSCopying, NSObject>)key;

- (BOOL)isEmpty;

- (NSUInteger)size;

- (NSArray *)keys;

- (NSUInteger)numberOfPuts;

- (NSUInteger)numberOfGets;

@end
