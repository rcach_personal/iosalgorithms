#import <Foundation/Foundation.h>

@interface RCFrequencyCounter : NSObject

+ (void)test:(NSUInteger)minLength inputFilePath:(NSString *)filePath;

@end