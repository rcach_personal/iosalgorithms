#import <Foundation/Foundation.h>

#import "RCSequentialSearchSymbolTable.h"

@interface RCBasicSymbolTableTestClient : NSObject

+ (void)test:(NSString *)input;

@end