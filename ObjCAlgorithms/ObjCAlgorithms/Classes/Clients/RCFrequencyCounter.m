#import "RCFrequencyCounter.h"

#import "RCSymbolTable.h"
#import "RCSequentialSearchSymbolTable.h"

@implementation RCFrequencyCounter

+ (void)test:(NSUInteger)minLength inputFilePath:(NSString *)filePath {
  id<RCSymbolTable> symbolTable = [RCSequentialSearchSymbolTable symbolTable];

  NSLog(@"Min Length %ld", minLength);
  
  // Build symbol table.
  NSArray *words = @[@"hello", @"my", @"name", @"is", @"Rene", @"Rene"];
  for (NSString *word in words) {
    if ([word length] < minLength) continue;
    if (![symbolTable containsKey:word]) {
      [symbolTable putKey:word value:@(1)];
    } else {
      NSUInteger wordCount = [[symbolTable getValueWithKey:word] intValue];
      [symbolTable putKey:word value:@(wordCount + 1)];
    }
  }
  // Find a key with the highest frequency count.
  NSString *max = @"";
  [symbolTable putKey:max value:@(0)];
  for (NSString *word in [symbolTable keys]) {
    NSUInteger thisWordsCount = [[symbolTable getValueWithKey:word] intValue];
    NSUInteger maxCount = [[symbolTable getValueWithKey:max] intValue];
    if (thisWordsCount > maxCount) {
      max = word;
    }
  }
  NSLog(@"%@ %@", max, [symbolTable getValueWithKey:max]);
  NSLog(@"Gets: %ld", [symbolTable numberOfGets]);
  NSLog(@"Puts: %ld", [symbolTable numberOfPuts]);
}

+ (NSArray *)standardInputWords {
  NSString *standardInputString = [self standardInputString];
  return [standardInputString componentsSeparatedByString:@" "];
}

+ (NSString *)standardInputString {
  NSFileHandle *input = [NSFileHandle fileHandleWithStandardInput];
  NSData *inputData = [NSData dataWithData:[input readDataToEndOfFile]];
  NSString *inputString = [[NSString alloc] initWithData:inputData
                                                encoding:NSUTF8StringEncoding];
  return inputString;
}



@end
