#import "RCBasicSymbolTableTestClient.h"

#import "RCSymbolTable.h"

@implementation RCBasicSymbolTableTestClient

+ (void)test:(NSString *)input {
  id<RCSymbolTable> symbolTable = [RCSequentialSearchSymbolTable symbolTable];
  
  for (int i = 0; i < [input length]; i++) {
    NSString *key = [NSString stringWithFormat:@"%c",
                     [input characterAtIndex:i]];
    [symbolTable putKey:key value:@(i)];
  }
  
  for (NSString *key in [symbolTable keys]) {
    NSLog(@"%@ : %@\n", key, [symbolTable getValueWithKey:key]);
  }
  
  NSLog(@"Gets: %ld", [symbolTable numberOfGets]);
  NSLog(@"Puts: %ld", [symbolTable numberOfPuts]);
}

@end
