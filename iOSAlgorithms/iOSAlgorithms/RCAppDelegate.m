#import "RCAppDelegate.h"

#import "RCViewController.h"

#import "RCBasicSymbolTableTestClient.h"
#import "RCFrequencyCounter.h"
#import "RCLinkedListBagClient.h"
#import "RCGraphFileInputClient.h"
#import "RCUnionFindClient.h"
#import "RCGraphSearchClient.h"
#import "RCGraphPathClient.h"
#import "RCConnectedComponentsClient.h"
#import "RCSymbolGraphClient.h"
#import "RCDegreesOfSeparationClient.h"
#import "RCDirectedDFSClient.h"

@implementation RCAppDelegate

- (BOOL)application:(UIApplication *)application
    didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
  
  dispatch_queue_t taskQ = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
  dispatch_async(taskQ, ^{
    [RCBasicSymbolTableTestClient test:@"SEARCHEXAMPLE"];
    
    /*
    NSString *inputPath = [[NSBundle mainBundle] pathForResource:@"leipzig1M"
                                                          ofType:@"txt"];
    [RCFrequencyCounter test:8 inputFilePath:inputPath];
     */
    
    RCLinkedListBagClient *llClient = [[RCLinkedListBagClient alloc] init];
    [llClient run];
    
    RCGraphFileInputClient *giClient = [[RCGraphFileInputClient alloc] init];
    [giClient run];
    
    RCUnionFindClient *ufClient = [[RCUnionFindClient alloc] init];
    [ufClient run];
    
    RCGraphSearchClient *gsClient = [[RCGraphSearchClient alloc] init];
    [gsClient run];
    
    RCGraphPathClient *gpClient = [[RCGraphPathClient alloc] init];
    [gpClient run];
    
    RCConnectedComponentsClient *ccClient =
        [[RCConnectedComponentsClient alloc] init];
    [ccClient run];
    
    RCSymbolGraphClient *sgClient = [[RCSymbolGraphClient alloc] init];
    [sgClient run];
    
    RCDegreesOfSeparationClient *dosClient =
        [[RCDegreesOfSeparationClient alloc] init];
//    [dosClient run];
    
    RCDirectedDFSClient *ddfsClient = [[RCDirectedDFSClient alloc] init];
    [ddfsClient run];
  });
  
  self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
  self.viewController = [[RCViewController alloc] initWithNibName:@"RCViewController"
                                                           bundle:nil];
  self.window.rootViewController = self.viewController;
  [self.window makeKeyAndVisible];
  return YES;
}

@end
