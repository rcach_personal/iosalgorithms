//
//  main.m
//  iOSAlgorithms
//
//  Created by Rene Cacheaux on 4/14/13.
//  Copyright (c) 2013 RCach. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RCAppDelegate.h"

int main(int argc, char *argv[])
{
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([RCAppDelegate class]));
  }
}
