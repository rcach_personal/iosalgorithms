#import <UIKit/UIKit.h>

@class RCViewController;

@interface RCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) RCViewController *viewController;

@end
