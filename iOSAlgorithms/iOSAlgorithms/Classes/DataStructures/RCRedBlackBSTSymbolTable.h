#import "RCOrderedSymbolTable.h"

@interface RCRedBlackBSTSymbolTable : NSObject<RCOrderedSymbolTable>

- (void)printByLevel;

- (BOOL)isValidBST;

@end
