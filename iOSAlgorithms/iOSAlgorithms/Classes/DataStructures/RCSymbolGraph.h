#import <Foundation/Foundation.h>

@class RCGraph;

@interface RCSymbolGraph : NSObject

+ (instancetype)symbolGraphWithPath:(NSString *)path
                          delimiter:(NSString *)delimiter;

- (id)initWithPath:(NSString *)path delimeter:(NSString *)delimiter;

- (BOOL)containsKey:(NSString *)key;
- (NSInteger)indexForKey:(NSString *)key;
- (NSString *)keyForIndex:(NSInteger)index;
- (RCGraph *)graph;

@end
