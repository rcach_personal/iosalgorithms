#import <Foundation/Foundation.h>

@interface RCUnionFind : NSObject

+ (instancetype)unionFindWithSize:(NSInteger)size;

- (id)initWithSize:(NSInteger)size;

- (void)addConnectionBetweenNode:(NSInteger)nodeA toNode:(NSInteger)nodeB;
- (NSInteger)componentIDForNode:(NSInteger)node;
- (BOOL)node:(NSInteger)nodeA connectedToNode:(NSInteger)nodeB;
- (NSInteger)numberOfComponents;

@end
