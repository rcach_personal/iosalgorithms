#import "RCDepthFirstGraphSearch.h"

#import "RCBag.h"
#import "RCGraph.h"

@interface RCDepthFirstGraphSearch ()
@property(nonatomic, strong) NSMutableArray *marked;
@property(nonatomic, assign) NSInteger numberOfNodesConnected;
@end


@implementation RCDepthFirstGraphSearch

+ (instancetype)searchWithGraph:(RCGraph *)graph sourceNode:(NSInteger)sourceNode {
  return [[self alloc] initWithGraph:graph sourceNode:sourceNode];
}

- (id)initWithGraph:(RCGraph *)graph sourceNode:(NSInteger)sourceNode {
  self = [super init];
  if (self) {
    _marked  = [NSMutableArray arrayWithCapacity:[graph numberOfVerticies]];
    for (NSInteger v = 0; v < [graph numberOfVerticies]; v++) {
      _marked[v] = @(NO);
    }
    [self depthFirstSearchWithGraph:graph node:sourceNode];
  }
  return self;
}

- (void)depthFirstSearchWithGraph:(RCGraph *)graph node:(NSInteger)node {
  if (node >= [self.marked count]) return;
  
  self.marked[node] = @(YES);
  self.numberOfNodesConnected++;
  NSEnumerator *adjacentEnumerator = [[graph verticiesAdjacentToVertex:node] enumerator];
  for (NSNumber *adjNode in adjacentEnumerator) {
    if (![self.marked[[adjNode integerValue]] boolValue]) {
      [self depthFirstSearchWithGraph:graph node:[adjNode integerValue]];
    }
  }
}

// Default initalizer not supported.
- (id)init {
  return [self initWithGraph:nil sourceNode:-1];
}

- (BOOL)nodeIsConnected:(NSInteger)node {
  if (node >= [self.marked count]) return NO;
  
  return [self.marked[node] boolValue];
}

@end
