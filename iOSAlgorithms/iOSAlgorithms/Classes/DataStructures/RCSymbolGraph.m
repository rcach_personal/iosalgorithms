#import "RCSymbolGraph.h"

#import "RCGraph.h"
#import "RCRedBlackBSTSymbolTable.h"

@interface RCSymbolGraph ()
@property(nonatomic, strong) RCRedBlackBSTSymbolTable *symbolTable;
@property(nonatomic, strong) NSMutableArray *keys;
@property(nonatomic, strong) RCGraph *graph;
@end

@implementation RCSymbolGraph

+ (instancetype)symbolGraphWithPath:(NSString *)path
                          delimiter:(NSString *)delimiter {
  return [[self alloc] initWithPath:path delimeter:delimiter];
}

- (id)initWithPath:(NSString *)path delimeter:(NSString *)delimiter {
  self = [super init];
  if (self) {
    NSString *contents = [NSString stringWithContentsOfFile:path
                                                   encoding:NSASCIIStringEncoding
                                                      error:NULL];
    NSArray *lines = [contents componentsSeparatedByCharactersInSet:
                      [NSCharacterSet newlineCharacterSet]];
    
    // First pass, build the symbol table and array of keys.
    _symbolTable = [RCRedBlackBSTSymbolTable symbolTable];
    for (NSString *line in lines) {
      NSArray *items = [line componentsSeparatedByString:delimiter];
      for (NSString<RCKey> *item in items) {
        if (![_symbolTable containsKey:item]) {
          [_symbolTable putKey:item value:@([_symbolTable size])];
        }
      }
    }
    _keys = [NSMutableArray arrayWithCapacity:[_symbolTable size]];
    for (NSInteger i = 0; i < [_symbolTable size]; i++) {
      _keys[i] = @"";
    }
    for (NSString<RCKey> *key in [_symbolTable keys]) {
      // TODO(rcacheaux): Perhaps guard against out of bounds.
      NSInteger index = [[_symbolTable getValueWithKey:key] integerValue];
      _keys[index] = key;
    }
    
    // Second pass build the graph.
    _graph = [RCGraph graphWithNumberOfVerticies:[_symbolTable size]];
    for (NSString *line in lines) {
      NSArray *items = [line componentsSeparatedByString:delimiter];
      if ([items count] > 0) {
        NSString<RCKey> *firstKey = items[0];
        NSInteger firstVertex =
            [[_symbolTable getValueWithKey:firstKey] integerValue];
        for (NSInteger i = 1; i < [items count]; i++) {
          NSString<RCKey> *key = items[i];
          NSInteger vertex = [[_symbolTable getValueWithKey:key] integerValue];
          [_graph addEdgeFromVertex:firstVertex toVertex:vertex];
        }
      }
    }
  }
  return self;
}

// Default initialzer not supported.
- (id)init {
  return [self initWithPath:nil delimeter:nil];
}

- (BOOL)containsKey:(NSString *)key {
  return [self.symbolTable containsKey:(NSString<RCKey> *)key];
}

- (NSInteger)indexForKey:(NSString *)key {
  return
      [[self.symbolTable getValueWithKey:(NSString<RCKey> *)key] integerValue];
}

- (NSString *)keyForIndex:(NSInteger)index {
  if (index >= [self.keys count]) return nil;
  
  return self.keys[index];
}

@end
