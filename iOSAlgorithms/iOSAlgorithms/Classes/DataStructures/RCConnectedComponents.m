#import "RCConnectedComponents.h"

#import "RCBag.h"
#import "RCGraph.h"

@interface RCConnectedComponents ()
@property(nonatomic, strong) NSMutableArray *marked;
@property(nonatomic, strong) NSMutableArray *componentIDs;
@property(nonatomic, assign) NSInteger numberOfConnectedComponents;
@end

@implementation RCConnectedComponents

+ (instancetype)connectedComponentsWithGraph:(RCGraph *)graph {
  return [[self alloc] initWithGraph:graph];
}

- (id)initWithGraph:(RCGraph *)graph {
  self = [super init];
  if (self) {
    _marked = [NSMutableArray arrayWithCapacity:[graph numberOfVerticies]];
    for (NSInteger v = 0; v < [graph numberOfVerticies]; v++) {
      _marked[v] = @(NO);
    }
    _componentIDs = [NSMutableArray arrayWithCapacity:[graph numberOfVerticies]];
    for (NSInteger v = 0; v < [graph numberOfVerticies]; v++) {
      _componentIDs[v] = @(-1);
    }
    
    for (NSInteger v = 0; v < [graph numberOfVerticies]; v++) {
      if (![self.marked[v] boolValue]) {
        [self depthFirstSearchGraph:graph node:v];
        self.numberOfConnectedComponents++;
      }
    }
  }
  return self;
}

// Default initializer not supported.
- (id)init {
  return [self initWithGraph:nil];
}

- (void)depthFirstSearchGraph:(RCGraph *)graph node:(NSInteger)node {
  if (node >= [self.componentIDs count]) return;
  
  self.marked[node] = @(YES);
  self.componentIDs[node] = @(self.numberOfConnectedComponents);
  NSEnumerator *adjNodes = [[graph verticiesAdjacentToVertex:node] enumerator];
  for (NSNumber *adjNode in adjNodes) {
    if (![self.marked[[adjNode integerValue]] boolValue]) {
      [self depthFirstSearchGraph:graph node:[adjNode integerValue]];
    }
  }
}

- (BOOL)node:(NSInteger)nodeA isConnectedToNode:(NSInteger)nodeB {
  if (nodeA >= [self.componentIDs count] || nodeB >= [self.componentIDs count]) {
    return NO;
  }
  return [self.componentIDs[nodeA] isEqual:self.componentIDs[nodeB]];
}

- (NSInteger)componentIDForNode:(NSInteger)node {
  if (node >= [self.componentIDs count]) return -1;
  
  return [self.componentIDs[node] integerValue];
}

@end
