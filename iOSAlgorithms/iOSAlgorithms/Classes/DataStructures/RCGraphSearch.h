#import <Foundation/Foundation.h>

@class RCGraph;

@protocol RCGraphSearch <NSObject>

+ (instancetype)searchWithGraph:(RCGraph *)graph sourceNode:(NSInteger)sourceNode;

- (BOOL)nodeIsConnected:(NSInteger)node;
- (NSInteger)numberOfNodesConnected;

@end
