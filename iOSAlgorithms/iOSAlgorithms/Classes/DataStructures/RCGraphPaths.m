#import "RCGraphPaths.h"

#import "RCGraphPaths_Protected.h"
#import "RCGraph.h"

#import "NSMutableArray+Stack.h"

@implementation RCGraphPaths

// Abstract class.
+ (instancetype)pathsWithGraph:(RCGraph *)graph sourceNode:(NSInteger)sourceNode {
  return [[self alloc] initWithGraph:graph sourceNode:sourceNode];
}

// Abstract class.
- (id)initWithGraph:(RCGraph *)graph sourceNode:(NSInteger)sourceNode {
  self = [super init];
  if (self) {
    _marked = [NSMutableArray arrayWithCapacity:[graph numberOfVerticies]];
    for (NSInteger v = 0; v < [graph numberOfVerticies]; v++) {
      _marked[v] = @(NO);
    }
    _edgeTo = [NSMutableArray arrayWithCapacity:[graph numberOfVerticies]];
    for (NSInteger v = 0; v < [graph numberOfVerticies];  v++) {
      _edgeTo[v] = @(-1);
    }
    _sourceNode = sourceNode;
  }
  return self;
}

// Default initializer not supported.
- (id)init {
  return [self initWithGraph:nil sourceNode:-1];
}

- (BOOL)hasPathToNode:(NSInteger)node {
  if (node >= [self.marked count]) return NO;
  
  return [self.marked[node] boolValue];
}

- (NSArray *)pathToNode:(NSInteger)node {
  if (node >= [self.edgeTo count]) return nil;
  if (![self hasPathToNode:node]) return nil;
  
  NSMutableArray *path = [NSMutableArray array];
  
  for (NSInteger x = node; x != self.sourceNode; x = [self.edgeTo[x] integerValue]) {
    [path rc_push:@(x)];
  }
  [path rc_push:@(self.sourceNode)];
  return [path copy];
}

@end
