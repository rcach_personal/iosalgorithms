#import <Foundation/Foundation.h>

#import "RCKey.h"
#import "RCSymbolTable.h"

@protocol RCOrderedSymbolTable <RCSymbolTable>

- (id<RCKey>)min;

- (id<RCKey>)max;

- (id<RCKey>)floorForKey:(id<RCKey>)key;

- (id<RCKey>)ceilingForKey:(id<RCKey>)key;

- (NSUInteger)rankForKey:(id<RCKey>)key;

- (id<RCKey>)keyOfRank:(NSUInteger)rank;
/*
- (void)deleteMin;

- (void)deleteMax;

- (NSUInteger)numberOfKeysFromLo:(NSUInteger)lo toHi:(NSUInteger)hi;

- (NSUInteger)keysWithinLo:(NSUInteger)lo toHi:(NSUInteger)hi;
*/
@end
