#import <Foundation/Foundation.h>

@interface NSMutableArray (Queue)

- (void)rc_enqueue:(id)object;

- (id)rc_dequeue;

@end
