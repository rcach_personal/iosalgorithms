#import "RCBreadthFirstGraphPaths.h"

#import "RCGraphPaths_Protected.h"

#import "RCBag.h"
#import "RCGraph.h"

#import "NSMutableArray+Queue.h"

@implementation RCBreadthFirstGraphPaths

- (id)initWithGraph:(RCGraph *)graph sourceNode:(NSInteger)sourceNode {
  self = [super initWithGraph:graph sourceNode:sourceNode];
  if (self) {
    [self breadthFirstSearchGraph:graph sourceNode:sourceNode];
  }
  return self;
}

- (void)breadthFirstSearchGraph:(RCGraph *)graph sourceNode:(NSInteger)sourceNode {
  if (sourceNode >= [self.marked count]) return;
  
  NSMutableArray *queue = [NSMutableArray array];
  self.marked[sourceNode] = @(YES);
  [queue rc_enqueue:@(sourceNode)];
  
  while ([queue count] > 0) {
    NSInteger node = [[queue rc_dequeue] integerValue];
    NSEnumerator *adjNodes = [[graph verticiesAdjacentToVertex:node] enumerator];
    for (NSNumber *adjNode in adjNodes) {
      if (![self.marked[[adjNode integerValue]] boolValue]) {
        self.marked[[adjNode integerValue]] = @(YES);
        self.edgeTo[[adjNode integerValue]] = @(node);
        [queue rc_enqueue:adjNode];
      }
    }
  }
}

@end
