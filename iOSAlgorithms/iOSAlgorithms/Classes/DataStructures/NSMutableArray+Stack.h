#import <Foundation/Foundation.h>

@interface NSMutableArray (Stack)

- (void)rc_push:(id)object;

- (id)rc_pop;

@end
