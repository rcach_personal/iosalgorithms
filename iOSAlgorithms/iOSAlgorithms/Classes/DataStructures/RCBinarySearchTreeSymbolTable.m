#import "RCBinarySearchTreeSymbolTable.h"

#import "NSMutableArray+Queue.h"

@interface BSTNode : NSObject
@end

@interface BSTNode ()
@property(nonatomic, strong) id<RCKey> key;
@property(nonatomic, strong) id value;
@property(nonatomic, strong) BSTNode *leftNode;
@property(nonatomic, strong) BSTNode *rightNode;
@property(nonatomic, assign) NSUInteger numberOfNodes;
@end

@implementation BSTNode

+ (instancetype)nodeWithKey:(id<RCKey>)key
                      value:(id)value
              numberOfNodes:(NSUInteger)numberOfNodes {
  BSTNode *node = [[self alloc] init];
  node.key = key;
  node.value = value;
  node.numberOfNodes = numberOfNodes;
  return node;
}

@end

@interface RCBinarySearchTreeSymbolTable ()
@property(nonatomic, strong) BSTNode *rootNode;
@property(nonatomic, assign) NSUInteger numberOfCompares;
@end

@implementation RCBinarySearchTreeSymbolTable

+ (instancetype)symbolTable {
  return [[self alloc] init];
}

- (NSUInteger)size {
  return [self sizeOfNode:self.rootNode];
}

- (id)getValueWithKey:(id<RCKey>)key {
  return [self getValueWithKey:key inTree:self.rootNode];
}

- (void)putKey:(id<RCKey>)key value:(id)value {
  self.rootNode = [self putKey:key value:value inTree:self.rootNode];
}

- (void)deleteValueWithKey:(id<RCKey>)key {
  // TODO: Implement.
}

- (BOOL)containsKey:(id<RCKey>)key {
  if ([self getValueWithKey:key inTree:self.rootNode]) {
    return YES;
  }
  return NO;
}

- (BOOL)isEmpty {
  if (self.rootNode) {
    return NO;
  }
  return YES;
}

- (NSArray *)keys {
  return [self keysFromLoKey:[self min] toHiKey:[self max]];
}

- (id<RCKey>)min {
  return [self minNodeForTree:self.rootNode].key;
}

- (id<RCKey>)max {
  return [self maxNodeForTree:self.rootNode].key;
}

- (id<RCKey>)floorForKey:(id<RCKey>)key {
  return [self floorNodeForKey:key inTree:self.rootNode].key;
}

- (id<RCKey>)ceilingForKey:(id<RCKey>)key {
  return [self ceilingNodeForKey:key inTree:self.rootNode].key;
}

- (NSUInteger)rankForKey:(id<RCKey>)key {
  return [self rankForKey:key inTree:self.rootNode];
}

- (id<RCKey>)keyOfRank:(NSUInteger)rank {
  return [self nodeForKeyOfRank:rank inTree:self.rootNode].key;
}

#pragma mark Private Implementations

- (NSUInteger)sizeOfNode:(BSTNode *)node {
  if (!node) {
    return 0;
  } else {
    return node.numberOfNodes;
  }
}

- (id)getValueWithKey:(id<RCKey>)key inTree:(BSTNode *)node {
  if (!node) {
    return nil;
  }
  NSComparisonResult comparison = [key compare:node.key];
  self.numberOfCompares++;
  if (comparison == NSOrderedAscending) {
    // Go to the left, key is smaller than this node's key.
    return [self getValueWithKey:key inTree:node.leftNode];
  } else if (comparison == NSOrderedDescending) {
    // Go to the right, key is greater than this node's key.
    return [self getValueWithKey:key inTree:node.rightNode];
  } else {
    // Key == this node's key, found the key so return value.
    return node.value;
  }
}

- (BSTNode *)putKey:(id<RCKey>)key value:(id)value inTree:(BSTNode *)node {
  if (!node) {
    // Found where this node goes. Create and return, caller will set this node
    // as either its left or right link.
    return [BSTNode nodeWithKey:key
                          value:value
                  numberOfNodes:1];
  }
  NSComparisonResult comparison = [key compare:node.key];
  self.numberOfCompares++;
  if (comparison == NSOrderedAscending) {
    // Key is less than this node's key so look to the left.
    node.leftNode = [self putKey:key value:value inTree:node.leftNode];
  } else if (comparison == NSOrderedDescending) {
    // Key is more than this node's key so look to the right.
    node.rightNode = [self putKey:key value:value inTree:node.rightNode];
  } else {
    // Found the key. Reset value.
    node.value = value;
  }
  node.numberOfNodes =
      [self sizeOfNode:node.leftNode] + [self sizeOfNode:node.rightNode] + 1;
  return node;
}

- (NSArray *)keysFromLoKey:(id<RCKey>)loKey toHiKey:(id<RCKey>)hiKey {
  NSMutableArray *keys = [NSMutableArray array];
  [self addKeyForNode:self.rootNode toKeys:keys loKey:loKey toHiKey:hiKey];
  return [keys copy];
}

- (void)addKeyForNode:(BSTNode *)node
               toKeys:(NSMutableArray *)keys
                loKey:(id<RCKey>)loKey
              toHiKey:(id<RCKey>)hiKey {
  if (node == nil) {
    return;
  }
  // Add all keys in left subtree, then add this node's key, and finally add
  // all the keys in the right subtree. Gaurd within lo and hi keys.
  NSComparisonResult loComparison = [node.key compare:loKey];
  self.numberOfCompares++;
  NSComparisonResult hiComparison = [node.key compare:hiKey];
  self.numberOfCompares++;
  if (loComparison == NSOrderedDescending) {
    [self addKeyForNode:node.leftNode toKeys:keys loKey:loKey toHiKey:hiKey];
  }
  if ((loComparison == NSOrderedDescending || loComparison == NSOrderedSame) &&
      (hiComparison == NSOrderedAscending || hiComparison == NSOrderedSame)) {
    [keys addObject:node.key];
  }
  if (hiComparison == NSOrderedAscending) {
    [self addKeyForNode:node.rightNode toKeys:keys loKey:loKey toHiKey:hiKey];
  }
}

- (BSTNode *)minNodeForTree:(BSTNode *)node {
  if (!node.leftNode) {
    // Can't go any smaller.
    return node;
  }
  return [self minNodeForTree:node.leftNode];
}

- (BSTNode *)maxNodeForTree:(BSTNode *)node {
  if (!node.rightNode) {
    // Can't go any larger.
    return node;
  }
  return [self maxNodeForTree:node.rightNode];
}

- (BSTNode *)floorNodeForKey:(id<RCKey>)key inTree:(BSTNode *)node {  
  if (!node) {
    // Gone too far. Key does not exist. Search miss.
    return nil;
  }
  NSComparisonResult comparison = [key compare:node.key];
  self.numberOfCompares++;
  if (comparison == NSOrderedSame) {
    return node;
  } else if (comparison == NSOrderedAscending) {
    // Key is less than this node's key, so look left.
    return [self floorNodeForKey:key inTree:node.leftNode];
  }
  // Look to the right, if the key is there return it, if not, this node
  // is the greatest node less than the key passed in.
  //
  // The key to look for is bigger than this node's key, but that key
  // may not exist and if it does not, this node then becomes the largest
  // possible key that is smaller that the would be key to look for.
  BSTNode *rightNode = [self floorNodeForKey:key inTree:node.rightNode];
  if (rightNode) {
    return rightNode;
  } else {
    return node;
  }
}

- (BSTNode *)ceilingNodeForKey:(id<RCKey>)key inTree:(BSTNode *)node {
  if (!node) {
    // Gone too far.
    return nil;
  }
  NSComparisonResult comparison = [key compare:node.key];
  self.numberOfCompares++;
  if (comparison == NSOrderedSame) {
    // Found the key, return.
    return node;
  } else if (comparison == NSOrderedDescending) {
    // Key to find is larger than this node's key so look right.
    return [self floorNodeForKey:key inTree:node.rightNode];
  }
  // This node is LARGER than the key to look for, so it's a candidate for
  // ceiling if the key doesn't exist.
  BSTNode *leftNode = [self floorNodeForKey:key inTree:node.leftNode];
  if (leftNode) {
    return leftNode;
  } else {
    return node;
  }
}

- (BSTNode *)nodeForKeyOfRank:(NSUInteger)rank inTree:(BSTNode *)node {
  if (!node) {
    // Gone to far.
    return nil;
  }
  NSInteger leftCount = node.leftNode.numberOfNodes;
  if (leftCount > rank) {
    // More than enough nodes to the left to know the node at rank is
    // most definitely to the left.
    return [self nodeForKeyOfRank:rank inTree:node.leftNode];
  } else if (leftCount < rank) {
    // Not enough nodes to the left, so node at rank is definitely to the right,
    // but need to compensate rank for moving to the right.
    return [self nodeForKeyOfRank:rank - leftCount - 1 inTree:node.rightNode];
  } else {
    // Rank and number of nodes to the left match, found node for key at rank.
    return node;
  }
}

- (NSInteger)rankForKey:(id<RCKey>)key inTree:(BSTNode *)node {
  if (!node) {
    return 0;
  }
  NSComparisonResult comparison = [key compare:node.key];
  self.numberOfCompares++;
  if (comparison == NSOrderedAscending) {
    // Look left.
    return [self rankForKey:key inTree:node.leftNode];
  } else if (comparison == NSOrderedDescending) {
    // Look right.
    return
        1 + [self sizeOfNode:node.leftNode] + [self rankForKey:key inTree:node.rightNode];
  } else {
    // Found node for key. Return number of nodes to left.
    return [self sizeOfNode:node.leftNode];
  }
}

- (void)printByLevel {
  NSMutableArray *queue = [[NSMutableArray alloc] init];
  [queue rc_enqueue:self.rootNode];
  BSTNode *newline = [BSTNode nodeWithKey:(id<RCKey>)@"newline"
                                    value:@"\n"
                            numberOfNodes:0];
  [queue rc_enqueue:newline];
  while ([queue count] > 0) {
    BSTNode *node = (BSTNode *)[queue rc_dequeue];
    if (node == newline) {
      NSLog(@"\n");
      if ([queue count] > 0) {
        [queue rc_enqueue:newline];
      }
    } else {
      NSLog(@"%@,", node.key);
      if (node.leftNode != nil) {
        [queue rc_enqueue:node.leftNode];
      }
      if (node.rightNode != nil) {
        [queue rc_enqueue:node.rightNode];
      }
    }
  }
}

- (BOOL)isValidBST {
  return [self isValidBSTWithNode:self.rootNode
                              min:(id<RCKey>)@"A"
                              max:(id<RCKey>)@"Z"];
}

- (BOOL)isValidBSTWithNode:(BSTNode *)node min:(id<RCKey>)min max:(id<RCKey>)max {
  if (node == nil) {
    return YES;
  }
  NSLog(@"Node: %@, MIN:%@, MAX:%@", node.key, min, max);
  NSComparisonResult minComparison = [node.key compare:min];
  NSComparisonResult maxComparison = [node.key compare:max];
  BOOL nodeGreaterThanMin = (minComparison == NSOrderedDescending || minComparison == NSOrderedSame);
  if (nodeGreaterThanMin) {
    NSLog(@"%@ > %@", node.key, min);
  } else {
    NSLog(@"!! %@ > %@", node.key, min);
  }
  BOOL nodeLessThanMax = (maxComparison == NSOrderedAscending || minComparison == NSOrderedSame);
  if (nodeLessThanMax) {
    NSLog(@"%@ < %@", node.key, max);
  } else {
    NSLog(@"!! %@ < %@", node.key, max);
  }
  if (nodeGreaterThanMin &&
      nodeLessThanMax &&
      [self isValidBSTWithNode:node.leftNode min:min max:node.key] &&
      [self isValidBSTWithNode:node.rightNode min:node.key max:max]) {
    return YES;
  } else {
    return NO;
  }
}

@end
