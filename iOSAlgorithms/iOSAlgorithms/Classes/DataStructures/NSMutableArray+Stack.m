#import "NSMutableArray+Stack.h"

@implementation NSMutableArray (Stack)

- (void)rc_push:(id)object {
  [self insertObject:object atIndex:0];
}

- (id)rc_pop {
  if ([self count] == 0) return nil;
  
  id firstObject = [self objectAtIndex:0];
  [self removeObjectAtIndex:0];
  return firstObject;
}

@end
