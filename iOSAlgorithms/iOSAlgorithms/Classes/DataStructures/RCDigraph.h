#import <Foundation/Foundation.h>

@protocol RCBag;

@interface RCDigraph : NSObject

// Create a V-vertex digraph with no edges.
+ (instancetype)digraphWithNumberOfVerticies:(NSInteger)numberOfVerticies;

// Read a digraph from file.
+ (instancetype)digraphFromInputFilePath:(NSString *)inputFilePath;

// Designated initializer.
- (id)initWithNumberOfVerticies:(NSInteger)numberOfVerticies;

- (NSInteger)numberOfVerticies;
- (NSInteger)numberOfEdges;

- (void)addEdgeFromVertex:(NSInteger)fromVertex toVertex:(NSInteger)toVertex;
- (id<RCBag>)verticiesAdjacentToVertex:(NSInteger)vertex;

- (RCDigraph *)reverseDigraph;

@end
