#import <Foundation/Foundation.h>

#import "RCKey.h"

@protocol RCSymbolTable <NSObject>

+ (instancetype)symbolTable;

- (void)putKey:(id<RCKey>)key value:(id)value;

// TODO: Change this to value for key.
- (id)getValueWithKey:(id<RCKey>)key;

- (void)deleteValueWithKey:(id<RCKey>)key;

- (BOOL)containsKey:(id<RCKey>)key;

- (BOOL)isEmpty;

- (NSUInteger)size;

- (NSArray *)keys;

- (NSUInteger)numberOfCompares;

@end
