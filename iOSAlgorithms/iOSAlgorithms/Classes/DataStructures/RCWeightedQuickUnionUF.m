#import "RCWeightedQuickUnionUF.h"

#import "RCUnionFind_Protected.h"

@interface RCWeightedQuickUnionUF ()
@property(nonatomic, strong) NSMutableArray *componentSizes;
@end

@implementation RCWeightedQuickUnionUF

- (id)initWithSize:(NSInteger)size {
  self = [super initWithSize:(NSInteger)size];
  if (self) {
    _componentSizes = [NSMutableArray arrayWithCapacity:size];
    for (NSInteger i = 0; i < size; i++) _componentSizes[i] = @(1);
  }
  return self;
}

- (void)addConnectionBetweenNode:(NSInteger)nodeA toNode:(NSInteger)nodeB {
  if (nodeA >= [self.componentIDs count] || nodeB >= [self.componentIDs count]) return;
  
  NSInteger componentIDA = [self componentIDForNode:nodeA];
  NSInteger componentIDB = [self componentIDForNode:nodeB];
  if (componentIDA == componentIDB) return;

  if (self.componentSizes[componentIDA] > self.componentSizes[componentIDB]) {
    // Tree rooted at A is larger than tree rooted at B, therefore add B to A.
    self.componentIDs[componentIDB] = @(componentIDA);
    // Tree rooted at A just grew by B's size.
    self.componentSizes[componentIDA] =
        @([self.componentSizes[componentIDA] integerValue] +
          [self.componentSizes[componentIDB] integerValue]);
  } else {
    self.componentIDs[componentIDA] = @(componentIDB);
    self.componentSizes[componentIDB] =
        @([self.componentSizes[componentIDB] integerValue] +
          [self.componentSizes[componentIDA] integerValue]);
  }
  self.numberOfComponents--;
}

@end
