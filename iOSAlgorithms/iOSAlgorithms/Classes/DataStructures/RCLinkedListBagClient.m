#import "RCLinkedListBagClient.h"

#import "RCLinkedListBag.h"

@implementation RCLinkedListBagClient

- (void)run {
  
  RCLinkedListBag *list = [RCLinkedListBag bag];
  [list addItem:@"Hello"];
  [list addItem:@"my"];
  [list addItem:@"name"];
  [list addItem:@"is"];
  [list addItem:@"Rene"];
  
  NSEnumerator *enumerator = [list enumerator];
  for (NSString *string in enumerator) {
    NSLog(@"%@\n", string);
  }
  
}

@end
