#import <Foundation/Foundation.h>

@class RCGraph;

// Abstract class.
@interface RCGraphPaths : NSObject

// Abstract class.
+ (instancetype)pathsWithGraph:(RCGraph *)graph sourceNode:(NSInteger)sourceNode;

// Abstract class.
- (id)initWithGraph:(RCGraph *)graph sourceNode:(NSInteger)sourceNode;

- (BOOL)hasPathToNode:(NSInteger)node;
- (NSArray *)pathToNode:(NSInteger)node;

@end
