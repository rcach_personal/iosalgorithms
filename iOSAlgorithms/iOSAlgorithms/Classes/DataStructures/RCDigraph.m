#import "RCDigraph.h"

#import "RCLinkedListBag.h"

@interface RCDigraph ()
@property(nonatomic, assign) NSInteger numberOfVerticies;
@property(nonatomic, assign) NSInteger numberOfEdges;
@property(nonatomic, strong) NSMutableArray *adjacencyLists;
@end

@implementation RCDigraph

+ (instancetype)digraphWithNumberOfVerticies:(NSInteger)numberOfVerticies {
  return [[self alloc] initWithNumberOfVerticies:numberOfVerticies];
}

+ (instancetype)digraphFromInputFilePath:(NSString *)inputFilePath {
  NSString *contents = [NSString stringWithContentsOfFile:inputFilePath
                                                 encoding:NSUTF8StringEncoding
                                                    error:NULL];
  NSArray *lines = [contents componentsSeparatedByCharactersInSet:
                    [NSCharacterSet newlineCharacterSet]];
  NSInteger numberOfVerticies = [lines[0] integerValue];
  NSInteger numberOfEdges = [lines[1] integerValue];
  
  RCDigraph *digraph =
      [RCDigraph digraphWithNumberOfVerticies:numberOfVerticies];
  
  for (NSInteger i = 2; i < [lines count]; i++) {
    NSArray *verticies = [lines[i] componentsSeparatedByCharactersInSet:
                          [NSCharacterSet whitespaceCharacterSet]];
    if ([verticies count] == 2) {
      [digraph addEdgeFromVertex:[verticies[0] integerValue]
                      toVertex:[verticies[1] integerValue]];
    } 
  }
  NSAssert(numberOfEdges == digraph.numberOfEdges, @"");
  return digraph;
}

- (id)init {
  return [self initWithNumberOfVerticies:0];
}

- (id)initWithNumberOfVerticies:(NSInteger)numberOfVerticies {
  self = [super init];
  if (self) {
    _numberOfVerticies = numberOfVerticies;
    _numberOfEdges = 0;
    _adjacencyLists = [NSMutableArray arrayWithCapacity:numberOfVerticies];
    for (NSInteger v = 0; v < numberOfVerticies; v++) {
      _adjacencyLists[v] = [RCLinkedListBag bag];
    }
  }
  return self;
}

- (void)addEdgeFromVertex:(NSInteger)fromVertex toVertex:(NSInteger)toVertex {
  if (fromVertex >= self.numberOfVerticies || fromVertex < 0 ||
      toVertex >= self.numberOfVerticies || toVertex < 0) {
    return;
  }
  [(id<RCBag>)self.adjacencyLists[fromVertex] addItem:@(toVertex)];
  self.numberOfEdges++;
}

- (id<RCBag>)verticiesAdjacentToVertex:(NSInteger)vertex {
  if (vertex >= self.numberOfVerticies || vertex < 0) {
    return nil;
  }
  return (id<RCBag>)self.adjacencyLists[vertex];
}

- (RCDigraph *)reverseDigraph {
  RCDigraph *digraph =
      [RCDigraph digraphWithNumberOfVerticies:self.numberOfVerticies];
  for (NSInteger v = 0; v < self.numberOfVerticies; v++) {
    NSEnumerator *adjVerticies =
        [[self verticiesAdjacentToVertex:v] enumerator];
    for (NSNumber *vertex in adjVerticies) {
      [digraph addEdgeFromVertex:[vertex integerValue] toVertex:v];
    }
  }
  return digraph;
}

- (NSString *)description {
  NSMutableString *description = [@"" mutableCopy];
  [description appendString:@"Graph: \n"];
  [description appendFormat:@"%d verticies, %d edges \n",
                            [self numberOfVerticies],
                            [self numberOfEdges]];
  for (NSInteger v = 0; v < [self numberOfVerticies]; v++) {
    [description appendFormat:@"%d: ", v];
    NSEnumerator *enumerator = [[self verticiesAdjacentToVertex:v] enumerator];
    for (NSNumber *vertex in enumerator) {
      [description appendFormat:@"-> %d ",[vertex integerValue]];
    }
    [description appendString:@"\n"];
  }
  return description;
}

@end
