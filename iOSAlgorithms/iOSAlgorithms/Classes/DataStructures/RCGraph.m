#import "RCGraph.h"

#import "RCLinkedListBag.h"

@interface RCGraph ()
@property(nonatomic, assign) NSInteger numberOfVerticies;
@property(nonatomic, assign) NSInteger numberOfEdges;
@property(nonatomic, strong) NSMutableArray *adjacencyLists;
@end

@implementation RCGraph

+ (instancetype)graphWithNumberOfVerticies:(NSInteger)numberOfVerticies {
  return [[self alloc] initWithNumberOfVerticies:numberOfVerticies];
}

+ (instancetype)graphFromInputFilePath:(NSString *)inputFilePath {
  NSString *contents = [NSString stringWithContentsOfFile:inputFilePath
                                                 encoding:NSUTF8StringEncoding
                                                    error:NULL];
  NSArray *lines = [contents componentsSeparatedByCharactersInSet:
                    [NSCharacterSet newlineCharacterSet]];
  NSInteger numberOfVerticies = [lines[0] integerValue];
  NSInteger numberOfEdges = [lines[1] integerValue];
  
  RCGraph *graph = [RCGraph graphWithNumberOfVerticies:numberOfVerticies];
  
  for (NSInteger i = 2; i < [lines count]; i++) {
    NSArray *verticies = [lines[i] componentsSeparatedByCharactersInSet:
                          [NSCharacterSet whitespaceCharacterSet]];
    if ([verticies count] == 2) {
      [graph addEdgeFromVertex:[verticies[0] integerValue]
                      toVertex:[verticies[1] integerValue]];
    } 
  }
  NSAssert(numberOfEdges == graph.numberOfEdges, @"");
  return graph;
}

- (id)init {
  return [self initWithNumberOfVerticies:0];
}

- (id)initWithNumberOfVerticies:(NSInteger)numberOfVerticies {
  self = [super init];
  if (self) {
    _numberOfVerticies = numberOfVerticies;
    _numberOfEdges = 0;
    _adjacencyLists = [NSMutableArray arrayWithCapacity:numberOfVerticies];
    for (NSInteger v = 0; v < numberOfVerticies; v++) {
      _adjacencyLists[v] = [RCLinkedListBag bag];
    }
  }
  return self;
}

- (void)addEdgeFromVertex:(NSInteger)fromVertex toVertex:(NSInteger)toVertex {
  if (fromVertex >= self.numberOfVerticies || fromVertex < 0 ||
      toVertex >= self.numberOfVerticies || toVertex < 0) {
    return;
  }
  [(id<RCBag>)self.adjacencyLists[fromVertex] addItem:@(toVertex)];
  [(id<RCBag>)self.adjacencyLists[toVertex] addItem:@(fromVertex)];
  self.numberOfEdges++;
}

- (id<RCBag>)verticiesAdjacentToVertex:(NSInteger)vertex {
  if (vertex >= self.numberOfVerticies || vertex < 0) {
    return nil;
  }
  return (id<RCBag>)self.adjacencyLists[vertex];
}

- (NSString *)description {
  NSMutableString *description = [@"" mutableCopy];
  [description appendString:@"Graph: \n"];
  [description appendFormat:@"%d verticies, %d edges \n",
                            [self numberOfVerticies],
                            [self numberOfEdges]];
  for (NSInteger v = 0; v < [self numberOfVerticies]; v++) {
    [description appendFormat:@"%d: ", v];
    NSEnumerator *enumerator = [[self verticiesAdjacentToVertex:v] enumerator];
    for (NSNumber *vertex in enumerator) {
      [description appendFormat:@"%d ",[vertex integerValue]];
    }
    [description appendString:@"\n"];
  }
  return description;
}

@end
