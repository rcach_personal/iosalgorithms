#import <Foundation/Foundation.h>

#import "RCOrderedSymbolTable.h"

@interface RCBinarySearchTreeSymbolTable : NSObject<RCOrderedSymbolTable>

- (void)printByLevel;

- (BOOL)isValidBST;

@end
