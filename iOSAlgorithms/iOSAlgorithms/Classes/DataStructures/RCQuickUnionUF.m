#import "RCQuickUnionUF.h"

#import "RCUnionFind_Protected.h"

@implementation RCQuickUnionUF

- (NSInteger)componentIDForNode:(NSInteger)node {
  if (node >= [self.componentIDs count]) return -1;
  NSInteger componentRootID = [self.componentIDs[node] integerValue];
  while ([self.componentIDs[componentRootID] integerValue] != componentRootID) {
    componentRootID = [self.componentIDs[componentRootID] integerValue];
  }
  return componentRootID;
}

- (void)addConnectionBetweenNode:(NSInteger)nodeA toNode:(NSInteger)nodeB {
  if (nodeA >= [self.componentIDs count] || nodeB >= [self.componentIDs count]) return;
  
  NSInteger componentIDA = [self componentIDForNode:nodeA];
  NSInteger componentIDB = [self componentIDForNode:nodeB];
  if (componentIDA == componentIDB) return;

  self.componentIDs[componentIDA] = @(componentIDB);
  self.numberOfComponents--;
}

@end
