#import "RCUnionFind.h"

@interface RCUnionFind ()

@property(nonatomic, strong) NSMutableArray *componentIDs;
@property(nonatomic, assign) NSInteger numberOfComponents;

@end
