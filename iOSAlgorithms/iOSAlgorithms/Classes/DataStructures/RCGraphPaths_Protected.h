#import "RCGraphPaths.h"

@interface RCGraphPaths ()

@property(nonatomic, strong) NSMutableArray *marked;
@property(nonatomic, strong) NSMutableArray *edgeTo;
@property(nonatomic, assign) NSInteger sourceNode;

@end
