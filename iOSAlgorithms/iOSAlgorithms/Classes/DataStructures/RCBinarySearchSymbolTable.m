#import "RCBinarySearchSymbolTable.h"

static const NSUInteger kArrayCapacity = 100000000;

@interface RCBinarySearchSymbolTable ()
@property(nonatomic, assign) NSMutableArray *keys;
@property(nonatomic, assign) NSMutableArray *values;
@property(nonatomic, assign) NSUInteger numberOfValues;
@property(nonatomic, assign, readwrite) NSUInteger numberOfCompares;
@end

@implementation RCBinarySearchSymbolTable

+ (instancetype)symbolTable {
  return [[self alloc] init];
}

- (id)init {
  self = [super init];
  if (self) {
    _keys = [NSMutableArray arrayWithCapacity:kArrayCapacity];
    _values = [NSMutableArray arrayWithCapacity:kArrayCapacity];
  }
  return self;
}

- (NSUInteger)size {
  return self.numberOfValues;
}

- (id)getValueWithKey:(id<RCKey>)key {
  if ([self isEmpty]) {
    return nil;
  }
  NSUInteger rank = [self rankForKey:key];
  self.numberOfCompares++;
  if (rank < self.numberOfValues && [key isEqual:self.keys[rank]]) {
    return self.values[rank];
  } else {
    return nil;
  }
}

- (NSUInteger)rankForKey:(id<RCKey>)key {
  NSInteger lo = 0;
  NSInteger hi = self.numberOfValues - 1;
  while (lo <= hi) {
    NSUInteger mid = lo + ((hi - lo) / 2);
    id<RCKey> midKey = self.keys[mid];
    self.numberOfCompares++;
    NSComparisonResult comparison = [key compare:midKey];
    if (comparison == NSOrderedDescending) {
      hi = mid - 1;
    } else if (comparison == NSOrderedAscending) {
      lo = mid + 1;
    } else {
      return mid;
    }
  }
  return lo;
}

- (void)putKey:(id<RCKey>)key value:(id)value {
  NSUInteger rank = [self rankForKey:key];
  self.numberOfCompares++;
  if (rank < self.numberOfValues && [key isEqual:self.keys[rank]]) {
    self.values[rank] = value;
    return;
  }
  // Shifts all elements larger than key one spot higher.
  for (int i = self.numberOfValues; i > rank; i--) {
    self.keys[i] = self.keys[i - 1];
    self.values[i] = self.values[i - 1];
  }
  self.keys[rank] = key;
  self.values[rank] = value;
  self.numberOfValues++;
}

- (void)deleteValueWithKey:(id<RCKey>)key {
  NSUInteger rank = [self rankForKey:key];
  self.numberOfCompares++;
  if (rank < self.numberOfValues && [key isEqual:self.keys[rank]]) {
    // Shift all elements in.
    for (int i = rank; i < self.numberOfValues - 1; i++) {
      self.keys[i] = self.keys[i + 1];
      self.values[i] = self.values[i + 1];
    }
    self.keys[self.numberOfValues - 1] = [NSNull null];
    self.values[self.numberOfValues - 1] = [NSNull null];
    self.numberOfValues--;
  }
  return;
}

- (id<RCKey>)min {
  return self.keys[0];
}

- (id<RCKey>)max {
  return self.keys[self.numberOfValues - 1];
}

- (id<RCKey>)keyOfRank:(NSUInteger)rank {
  if (rank < self.numberOfValues) {
    return self.keys[rank];
  }
  return nil;
}

- (id<RCKey>)ceilingForKey:(id<RCKey>)key {
  NSUInteger rank = [self rankForKey:key];
  return self.keys[rank];
}

- (id<RCKey>)floorForKey:(id<RCKey>)key {
  NSUInteger rank = [self rankForKey:key];
  self.numberOfCompares++;
  if ([key isEqual:self.keys[rank]]) {
    return self.keys[rank];
  }
  return self.keys[rank - 1];
}

- (BOOL)containsKey:(id<RCKey>)key {
  NSUInteger rank = [self rankForKey:key];
  self.numberOfCompares++;
  if (rank < self.numberOfValues && [key isEqual:self.keys[rank]]) {
    return YES;
  }
  return NO;
}

- (BOOL)isEmpty {
  return self.numberOfValues == 0;
}

@end
