#import <Foundation/Foundation.h>

@protocol RCBag;

// Graph backed by a adjacency-lists data structure intended for sparse graphs.
@interface RCGraph : NSObject

// Create a V-vertex graph with no edges.
+ (instancetype)graphWithNumberOfVerticies:(NSInteger)numberOfVerticies;

// Read a graph from file.
+ (instancetype)graphFromInputFilePath:(NSString *)inputFilePath;

// Designated initializer.
- (id)initWithNumberOfVerticies:(NSInteger)numberOfVerticies;

- (NSInteger)numberOfVerticies;
- (NSInteger)numberOfEdges;

- (void)addEdgeFromVertex:(NSInteger)fromVertex toVertex:(NSInteger)toVertex;
- (id<RCBag>)verticiesAdjacentToVertex:(NSInteger)vertex;

@end
