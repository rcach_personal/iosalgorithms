@protocol RCComparable <NSObject>

- (NSComparisonResult)compare:(id)anotherObject;

@end

@protocol RCKey <NSCopying, NSObject, RCComparable>
@end