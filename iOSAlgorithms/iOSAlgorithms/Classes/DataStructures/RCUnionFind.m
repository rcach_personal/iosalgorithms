#import "RCUnionFind.h"

#import "RCUnionFind_Protected.h"

@implementation RCUnionFind

+ (instancetype)unionFindWithSize:(NSInteger)size {
  return [[self alloc] initWithSize:size];
}

- (id)init {
  return [self initWithSize:0];
}

- (id)initWithSize:(NSInteger)size {
  self = [super init];
  if (self) {
    _componentIDs = [NSMutableArray arrayWithCapacity:size];
    for (NSInteger i = 0; i < size; i++) {
      _componentIDs[i] = @(i);
    }
    _numberOfComponents = size;
  }
  return self;
}

- (BOOL)node:(NSInteger)nodeA connectedToNode:(NSInteger)nodeB {
  return [self componentIDForNode:nodeA] == [self componentIDForNode:nodeB];
}

- (NSInteger)componentIDForNode:(NSInteger)node {
  // Abstract method.
  return -1;
}

- (void)addConnectionBetweenNode:(NSInteger)nodeA toNode:(NSInteger)nodeB {
  // Abstract method.
}

@end
