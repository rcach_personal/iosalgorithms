#import <Foundation/Foundation.h>

@protocol RCBag <NSObject>

// Returns an empty bag.
+ (id<RCBag>)bag;

- (void)addItem:(id)item;
- (BOOL)isEmpty;
- (NSInteger)size;
- (NSEnumerator *)enumerator;

@end
