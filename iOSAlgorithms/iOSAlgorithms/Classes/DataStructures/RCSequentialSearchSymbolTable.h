#import <Foundation/Foundation.h>

#import "RCSymbolTable.h"

@interface RCSequentialSearchSymbolTable : NSObject<RCSymbolTable>
@property(nonatomic, assign, readonly) NSUInteger numberOfCompares;
@end
