#import "RCDirectedDFS.h"

#import "RCBag.h"
#import "RCDigraph.h"

@interface RCDirectedDFS ()
@property(nonatomic, strong) NSMutableArray *marked;
@end

@implementation RCDirectedDFS

+ (instancetype)searchWithDigraph:(RCDigraph *)digraph
                       sourceNode:(NSInteger)sourceNode {
  return [[self alloc] initWithDigraph:digraph sourceNode:sourceNode];
}

+ (instancetype)searchWithDigraph:(RCDigraph *)digraph
                      sourceNodes:(NSArray *)sourceNodes {
  return [[self alloc] initWithDigraph:digraph sourceNodes:sourceNodes];
}

- (id)initWithDigraph:(RCDigraph *)digraph sourceNode:(NSInteger)sourceNode {
  self = [super init];
  if (self) {
    _marked  = [NSMutableArray arrayWithCapacity:[digraph numberOfVerticies]];
    for (NSInteger v = 0; v < [digraph numberOfVerticies]; v++) {
      _marked[v] = @(NO);
    }
    [self depthFirstSearchWithGraph:digraph node:sourceNode];
  }
  return self;
}

- (id)initWithDigraph:(RCDigraph *)digraph sourceNodes:(NSArray *)sourceNodes {
  self = [super init];
  if (self) {
    _marked  = [NSMutableArray arrayWithCapacity:[digraph numberOfVerticies]];
    for (NSInteger v = 0; v < [digraph numberOfVerticies]; v++) {
      _marked[v] = @(NO);
    }
    for (NSNumber *sourceNode in sourceNodes) {
      if (![self.marked[[sourceNode integerValue]] boolValue]) {
        [self depthFirstSearchWithGraph:digraph node:[sourceNode integerValue]];
      }
    }
  }
  return self;
}

- (void)depthFirstSearchWithGraph:(RCDigraph *)digraph node:(NSInteger)node {
  if (node >= [self.marked count]) return;
  
  self.marked[node] = @(YES);
  NSEnumerator *adjacentEnumerator =
      [[digraph verticiesAdjacentToVertex:node] enumerator];
  for (NSNumber *adjNode in adjacentEnumerator) {
    if (![self.marked[[adjNode integerValue]] boolValue]) {
      [self depthFirstSearchWithGraph:digraph node:[adjNode integerValue]];
    }
  }
}

// Default initalizer not supported.
- (id)init {
  return [self initWithDigraph:nil sourceNode:-1];
}

- (BOOL)nodeIsConnected:(NSInteger)node {
  if (node >= [self.marked count]) return NO;
  
  return [self.marked[node] boolValue];
}

@end
