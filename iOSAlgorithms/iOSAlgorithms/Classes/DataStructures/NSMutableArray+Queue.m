#import "NSMutableArray+Queue.h"

@implementation NSMutableArray (Queue)

- (void)rc_enqueue:(id)object {
  [self addObject:object];
}

- (id)rc_dequeue {
  id headObject = [self objectAtIndex:0];
  if (headObject != nil) {
    [self removeObjectAtIndex:0];
  }
  return headObject;
}

@end
