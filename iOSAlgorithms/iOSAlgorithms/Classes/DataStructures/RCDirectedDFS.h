#import <Foundation/Foundation.h>

@class RCDigraph;

@interface RCDirectedDFS : NSObject

+ (instancetype)searchWithDigraph:(RCDigraph *)digraph
                       sourceNode:(NSInteger)sourceNode;
+ (instancetype)searchWithDigraph:(RCDigraph *)digraph
                      sourceNodes:(NSArray *)sourceNodes;

- (BOOL)nodeIsConnected:(NSInteger)node;

@end
