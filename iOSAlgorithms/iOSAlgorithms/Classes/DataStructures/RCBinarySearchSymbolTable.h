#import <Foundation/Foundation.h>

#import "RCOrderedSymbolTable.h"

@interface RCBinarySearchSymbolTable : NSObject<RCOrderedSymbolTable>
@property(nonatomic, assign, readonly) NSUInteger numberOfCompares;

@end
