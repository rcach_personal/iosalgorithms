#import <Foundation/Foundation.h>

@class RCGraph;

@interface RCConnectedComponents : NSObject

+ (instancetype)connectedComponentsWithGraph:(RCGraph *)graph;

- (id)initWithGraph:(RCGraph *)graph;

- (BOOL)node:(NSInteger)nodeA isConnectedToNode:(NSInteger)nodeB;
- (NSInteger)numberOfConnectedComponents;
- (NSInteger)componentIDForNode:(NSInteger)node;

@end
