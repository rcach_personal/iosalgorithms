#import "RCDepthFirstGraphPaths.h"

#import "RCBag.h"
#import "RCGraph.h"

@implementation RCDepthFirstGraphPaths

- (id)initWithGraph:(RCGraph *)graph sourceNode:(NSInteger)sourceNode {
  self = [super initWithGraph:graph sourceNode:sourceNode];
  if (self) {
    [self depthFirstSearchGraph:graph node:sourceNode];
  }
  return self;
}

- (void)depthFirstSearchGraph:(RCGraph *)graph node:(NSInteger)node {
  if (node >= [self.marked count]) return;
  
  self.marked[node] = @(YES);
  
  NSEnumerator *adjNodes = [[graph verticiesAdjacentToVertex:node] enumerator];
  for (NSNumber *adjNode in adjNodes) {
    if (![self.marked[[adjNode integerValue]] boolValue]) {
      self.edgeTo[[adjNode integerValue]] = @(node);
      [self depthFirstSearchGraph:graph node:[adjNode integerValue]];
    }
  }
}

@end
