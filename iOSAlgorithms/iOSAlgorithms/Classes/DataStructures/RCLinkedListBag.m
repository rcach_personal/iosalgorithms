#import "RCLinkedListBag.h"

@interface RCLLNode : NSObject
@property(nonatomic, strong) id item;
@property(nonatomic, strong) RCLLNode *next;
@end

@interface RCLinkedListBagEnumerator : NSEnumerator
@property(nonatomic, strong) RCLLNode *current;
+ (instancetype)enumeratorForList:(RCLinkedListBag *)list;
@end

@interface RCLinkedListBag ()
@property(nonatomic, strong) RCLLNode *first;
@property(nonatomic, assign) NSInteger size;
@end

@implementation RCLinkedListBag

+ (instancetype)bag {
  return [[self alloc] init];
}

- (id)init {
  self = [super init];
  if (self) {
    _size = 0;
  }
  return self;
}

- (void)addItem:(id)item {
  RCLLNode *oldFirst = self.first;
  self.first = [[RCLLNode alloc] init];
  self.first.item = item;
  self.first.next = oldFirst;
  self.size++;
}

- (BOOL)isEmpty {
  return self.first == nil;
}

- (NSEnumerator *)enumerator {
  return [RCLinkedListBagEnumerator enumeratorForList:self];
}

@end



@implementation RCLLNode

@end


@implementation RCLinkedListBagEnumerator

+ (instancetype)enumeratorForList:(RCLinkedListBag *)list {
  RCLinkedListBagEnumerator *enumerator =
      [[RCLinkedListBagEnumerator alloc] init];
  enumerator.current = list.first;
  return enumerator;
}

- (NSArray *)allObjects {
  NSMutableArray *objectsRemaining = [NSMutableArray array];
  RCLLNode *node = self.current;
  while (node != nil) {
    [objectsRemaining addObject:node.item];
    node = node.next;
  }
  return [objectsRemaining copy];
}

- (id)nextObject {
  id item = self.current.item;
  self.current = self.current.next;
  return item;
}

@end
