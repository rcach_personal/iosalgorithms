#import "RCQuickFindUF.h"

#import "RCUnionFind_Protected.h"

@implementation RCQuickFindUF

- (NSInteger)componentIDForNode:(NSInteger)node {
  if (node >= [self.componentIDs count]) return -1;
  return [self.componentIDs[node] integerValue];
}

- (void)addConnectionBetweenNode:(NSInteger)nodeA toNode:(NSInteger)nodeB {
  if (nodeA >= [self.componentIDs count] || nodeB >= [self.componentIDs count]) return;

  NSInteger componentIDA = [self componentIDForNode:nodeA];
  NSInteger componentIDB = [self componentIDForNode:nodeB];
  if (componentIDA == componentIDB) return;

  for (NSInteger i = 0; i < [self.componentIDs count]; i++) {
    if ([self.componentIDs[i] integerValue] == componentIDA) {
      self.componentIDs[i] = @(componentIDB);
    }
  }
  self.numberOfComponents--;
}

@end
