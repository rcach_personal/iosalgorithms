#import "RCSequentialSearchSymbolTable.h"

static const BOOL kEagerDelete = NO;

@interface RCNode : NSObject

@property(nonatomic, copy) id<NSCopying> key;
@property(nonatomic, strong) id value;
@property(nonatomic, strong) RCNode *next;

+ (instancetype)nodeWithKey:(id<RCKey>)key
                      value:(id)value
                   nextNode:(RCNode *)next;

@end

@implementation RCNode

+ (instancetype)nodeWithKey:(id<RCKey>)key
                      value:(id)value
                   nextNode:(RCNode *)next {
  RCNode *node = [[RCNode alloc] init];
  node.key = key;
  node.value = value;
  node.next = next;
  return node;
}

@end

@interface RCSequentialSearchSymbolTable ()
@property(nonatomic, strong) RCNode *rootNode;
@property(nonatomic, assign, readwrite) NSUInteger numberOfCompares;
@end

@implementation RCSequentialSearchSymbolTable

+ (instancetype)symbolTable {
  return [[self alloc] init];
}

- (id)getValueWithKey:(id<RCKey>)key {
  for (RCNode *x = self.rootNode; x; x = x.next) {
    self.numberOfCompares++;
    if ([key isEqual:x.key]) {
      return x.value;
    }
  }
  return nil;
}

- (void)putKey:(id<RCKey>)key value:(id)value {
  for (RCNode *x = self.rootNode; x; x = x.next) {
    self.numberOfCompares++;
    if ([key isEqual:x.key]) {
      x.value = value;
      return;
    }
  }
  self.rootNode = [RCNode nodeWithKey:key value:value nextNode:self.rootNode];
}

- (NSUInteger)size {
  NSUInteger size = 0;
  for (RCNode *x = self.rootNode; x; x = x.next) {
    // Guard is for lazy delete implementations.
    if (x.value) {
      size++;
    }
  }
  return size;
}

- (BOOL)containsKey:(id<RCKey>)key {
  return [self getValueWithKey:key] != nil;
}


- (void)deleteValueWithKey:(id<RCKey>)key {
  if (kEagerDelete) {
    RCNode *previousNode = nil;
    for (RCNode *x = self.rootNode; x; x = x.next) {
      self.numberOfCompares++;
      if ([key isEqual:x.key]) {
        // Found node to delete. Fix links.
        if (x == self.rootNode) {
          self.rootNode = x.next;
          return;
        } else {
          previousNode.next = x.next;
          return;
        }
      }
      previousNode = x;
    }
  } else {
    // Lazy delete implementation.
    [self putKey:key value:nil];
  }
}

- (NSArray *)keys {
  NSMutableArray *keys = [NSMutableArray array];
  for (RCNode *x = self.rootNode; x; x = x.next) {
    // Guard is for lazy delete implementations.
    if (x.value) {
      [keys addObject:x.key];
    }
  }
  return [keys copy];
}

- (BOOL)isEmpty {
  return [self size] == 0;
}

@end
