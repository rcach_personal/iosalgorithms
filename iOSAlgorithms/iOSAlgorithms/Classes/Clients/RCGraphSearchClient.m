#import "RCGraphSearchClient.h"

#import "RCGraph.h"
#import "RCDepthFirstGraphSearch.h"

@implementation RCGraphSearchClient

- (void)run {
  NSString *filePath = [[NSBundle mainBundle] pathForResource:@"tinyG" ofType:@"txt"];
  RCGraph *graph = [RCGraph graphFromInputFilePath:filePath];
  NSInteger sourceNode = 9;
  RCDepthFirstGraphSearch *search =
      [RCDepthFirstGraphSearch searchWithGraph:graph sourceNode:sourceNode];
  
  for (NSInteger v = 0; v < [graph numberOfVerticies]; v++) {
    if ([search nodeIsConnected:v]) {
      NSLog(@"%d ", v);
    }
  }
  if ([search numberOfNodesConnected] != [graph numberOfVerticies]) {
    NSLog(@"IS NOT ");
  }
  NSLog(@"Connected");
}

@end
