#import "RCConnectedComponentsClient.h"

#import "RCGraph.h"
#import "RCConnectedComponents.h"

#import "NSMutableArray+Queue.h"

@implementation RCConnectedComponentsClient

- (void)run {
  NSString *filePath = [[NSBundle mainBundle] pathForResource:@"tinyG" ofType:@"txt"];
  RCGraph *graph = [RCGraph graphFromInputFilePath:filePath];
  RCConnectedComponents *connectedComponents =
      [RCConnectedComponents connectedComponentsWithGraph:graph];
  
  NSInteger numberOfComponents = [connectedComponents numberOfConnectedComponents];
  NSLog(@"%d components", numberOfComponents);
  
  NSMutableArray *components = [NSMutableArray arrayWithCapacity:numberOfComponents];
  for (NSInteger i = 0; i < numberOfComponents; i++) {
    components[i] = [NSMutableArray array];
  }
  for (NSInteger v = 0; v < [graph numberOfVerticies]; v++) {
    NSInteger componentID = [connectedComponents componentIDForNode:v];
    // TODO(rcacheaux): Error handling in case component ID unexpectedly is out of bounds
    // for components.
    NSMutableArray *queue = components[componentID];
    [queue rc_enqueue:@(v)];
  }
  for (NSInteger i = 0; i < numberOfComponents; i++) {
    NSMutableString *output = [@"" mutableCopy];
    for (NSNumber *node in components[i]) {
      [output appendFormat:@"%@ ", node];
    }
    [output appendString:@"\n"];
    NSLog(@"%@", output);
  }
}



@end
