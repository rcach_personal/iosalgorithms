#import "RCDirectedDFSClient.h"

#import "RCDigraph.h"
#import "RCDirectedDFS.h"

@implementation RCDirectedDFSClient

- (void)run {
  NSString *filePath = [[NSBundle mainBundle] pathForResource:@"tinyDG"
                                                       ofType:@"txt"];
  RCDigraph *digraph = [RCDigraph digraphFromInputFilePath:filePath];
  NSArray *sourceNodes = @[@(1),@(2),@(6)];
  
  RCDirectedDFS *reachable = [RCDirectedDFS searchWithDigraph:digraph
                                                  sourceNodes:sourceNodes];
  
  for (NSInteger v = 0; v < [digraph numberOfVerticies]; v++) {
    if ([reachable nodeIsConnected:v]) {
      NSLog(@"%d ", v);
    }
  }
}

@end
