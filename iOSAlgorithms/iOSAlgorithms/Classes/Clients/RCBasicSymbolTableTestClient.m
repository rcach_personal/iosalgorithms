#import "RCBasicSymbolTableTestClient.h"

#import "RCSymbolTable.h"
#import "RCSequentialSearchSymbolTable.h"
#import "RCBinarySearchSymbolTable.h"
#import "RCBinarySearchTreeSymbolTable.h"
#import "RCRedBlackBSTSymbolTable.h"

@implementation RCBasicSymbolTableTestClient

+ (void)test:(NSString *)inputString {
  CFAbsoluteTime totalStartTime = CFAbsoluteTimeGetCurrent();
  NSLog(@"- Basic Symbol Table Test -");
  id<RCSymbolTable> symbolTable = [RCBinarySearchTreeSymbolTable symbolTable];
  
  CFAbsoluteTime populateStartTime = CFAbsoluteTimeGetCurrent();
  for (int i = 0; i < [inputString length]; i++) {
    NSString<RCKey> *key = [NSString stringWithFormat:@"%c",
                     [inputString characterAtIndex:i]];
    [symbolTable putKey:key value:@(i)];
  }
  NSLog(@"Populate Time: %.2fms",
        1000.0f * (CFAbsoluteTimeGetCurrent() - populateStartTime));
  
  CFAbsoluteTime accessStartTime = CFAbsoluteTimeGetCurrent();
  for (NSString<RCKey> *key in [symbolTable keys]) {
    NSLog(@"%@ : %@\n", key, [symbolTable getValueWithKey:key]);
  }
  NSLog(@"Access Time: %.2fms",
        1000.0f * (CFAbsoluteTimeGetCurrent() - accessStartTime));
  
  NSLog(@"Compares: %ld", (unsigned long)[symbolTable numberOfCompares]);
  NSLog(@"Time: %.2fms", 1000.0f * (CFAbsoluteTimeGetCurrent() - totalStartTime));
  NSLog(@"\n");
  
  RCBinarySearchTreeSymbolTable *bst = (RCBinarySearchTreeSymbolTable *)symbolTable;
  [bst printByLevel];
  if ([bst isValidBST]) {
    NSLog(@"Is valid");
  } else {
    NSLog(@"NOT valid");
  }
  
}

@end
