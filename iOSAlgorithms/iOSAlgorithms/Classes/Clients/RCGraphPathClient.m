#import "RCGraphPathClient.h"

#import "RCGraph.h"
#import "RCDepthFirstGraphPaths.h"
#import "RCBreadthFirstGraphPaths.h"

// Single source path client.
@implementation RCGraphPathClient

- (void)run {
  NSString *filePath = [[NSBundle mainBundle] pathForResource:@"tinyCG" ofType:@"txt"];
  RCGraph *graph = [RCGraph graphFromInputFilePath:filePath];
  NSInteger sourceNode = 0;
//  RCDepthFirstGraphPaths *paths =
//      [RCDepthFirstGraphPaths pathsWithGraph:graph sourceNode:sourceNode];
  RCBreadthFirstGraphPaths *paths =
      [RCBreadthFirstGraphPaths pathsWithGraph:graph sourceNode:sourceNode];
  
  for (NSInteger v = 0; v < [graph numberOfVerticies]; v++) {
    if ([paths hasPathToNode:v]) {
      NSMutableString *output = [@"" mutableCopy];
      for (NSNumber *node in [paths pathToNode:v]) {
        if ([node integerValue] == sourceNode) [output appendFormat:@"%@", node];
        else [output appendFormat:@" - %@", node];
      }
      [output appendString:@"\n"];
      NSLog(@"%@", output);
    }
  }
}

@end
