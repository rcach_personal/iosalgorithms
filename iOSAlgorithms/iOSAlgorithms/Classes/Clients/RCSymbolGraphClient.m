#import "RCSymbolGraphClient.h"

#import "RCBag.h"
#import "RCGraph.h"
#import "RCSymbolGraph.h"

@implementation RCSymbolGraphClient

- (void)run {
  NSString *filePath = [[NSBundle mainBundle] pathForResource:@"routes"
                                                       ofType:@"txt"];
  NSString *delimiter = @" ";
  RCSymbolGraph *symbolGraph = [RCSymbolGraph symbolGraphWithPath:filePath
                                                        delimiter:delimiter];
  RCGraph *graph = [symbolGraph graph];
  
  NSString *source = @"LAX";
  NSLog(@"%@ \n", source);
  NSInteger index = [symbolGraph indexForKey:source];
  NSEnumerator *enumerator =
      [[graph verticiesAdjacentToVertex:index] enumerator];
  for (NSNumber *vertex in enumerator) {
    NSLog(@"  %@", [symbolGraph keyForIndex:[vertex integerValue]]);
  }
}

@end
