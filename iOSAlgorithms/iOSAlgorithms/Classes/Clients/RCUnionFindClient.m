#import "RCUnionFindClient.h"

#import "RCQuickFindUF.h"
#import "RCQuickUnionUF.h"
#import "RCWeightedQuickUnionUF.h"

@implementation RCUnionFindClient

- (void)run {
  
  
  
  NSString *filePath = [[NSBundle mainBundle] pathForResource:@"tinyUF" ofType:@"txt"];
  NSString *contents = [NSString stringWithContentsOfFile:filePath
                                                 encoding:NSUTF8StringEncoding
                                                    error:NULL];
  
  NSArray *lines = [contents componentsSeparatedByCharactersInSet:
                    [NSCharacterSet newlineCharacterSet]];
  
  CFAbsoluteTime accessStartTime = CFAbsoluteTimeGetCurrent();
  NSInteger size = [lines[0] integerValue];
  
//  RCQuickFindUF *unionFind = [RCQuickFindUF unionFindWithSize:size];
//  RCQuickUnionUF *unionFind = [RCQuickUnionUF unionFindWithSize:size];
  RCWeightedQuickUnionUF *unionFind = [RCWeightedQuickUnionUF unionFindWithSize:size];
  
  for (NSInteger i = 1; i < [lines count]; i++) {
    NSArray *linkedNodes = [lines[i] componentsSeparatedByCharactersInSet:
                          [NSCharacterSet whitespaceCharacterSet]];
    if ([linkedNodes count] == 2) {
      NSInteger nodeA = [linkedNodes[0] integerValue];
      NSInteger nodeB = [linkedNodes[1] integerValue];
      if ([unionFind node:nodeA connectedToNode:nodeB]) continue;
      [unionFind addConnectionBetweenNode:nodeA toNode:nodeB];
      NSLog(@"%d %d", nodeA, nodeB);
    }
  }
  NSLog(@"%d components \n", [unionFind numberOfComponents]);
  NSLog(@"Access Time: %.2fms \n",
        1000.0f * (CFAbsoluteTimeGetCurrent() - accessStartTime));
}

@end
