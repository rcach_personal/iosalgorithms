#import "RCFrequencyCounter.h"

#import "RCSymbolTable.h"
#import "RCSequentialSearchSymbolTable.h"
#import "RCBinarySearchSymbolTable.h"
#import "RCBinarySearchTreeSymbolTable.h"

@implementation RCFrequencyCounter

+ (void)test:(NSUInteger)minLength inputFilePath:(NSString *)inputFilePath {
  CFAbsoluteTime totalStartTime = CFAbsoluteTimeGetCurrent();
  
  NSLog(@"- Frequency Counter Symbol Table Test -");
  id<RCSymbolTable> symbolTable = [RCBinarySearchTreeSymbolTable symbolTable];

  NSLog(@"Min Length %ld", (unsigned long)minLength);
  
  // Build symbol table.
  
  NSArray *words = [self inputWordsWithPath:inputFilePath];
  CFAbsoluteTime populateStartTime = CFAbsoluteTimeGetCurrent();
  for (NSString<RCKey> *word in words) {
    if ([word length] < minLength) continue;
    if (![symbolTable containsKey:word]) {
      [symbolTable putKey:word value:@(1)];
    } else {
      NSUInteger wordCount = [[symbolTable getValueWithKey:word] intValue];
      [symbolTable putKey:word value:@(wordCount + 1)];
    }
  }
  NSLog(@"Populate Time: %.2fms",
        1000.0f * (CFAbsoluteTimeGetCurrent() - populateStartTime));
  
  // Find a key with the highest frequency count.
  CFAbsoluteTime accessStartTime = CFAbsoluteTimeGetCurrent();
  NSString<RCKey> *max = (NSString<RCKey> *)@"";
  [symbolTable putKey:max value:@(0)];
  for (NSString<RCKey> *word in [symbolTable keys]) {
    NSUInteger thisWordsCount = [[symbolTable getValueWithKey:word] intValue];
    NSUInteger maxCount = [[symbolTable getValueWithKey:max] intValue];
    if (thisWordsCount > maxCount) {
      max = word;
    }
  }
  
  NSLog(@"%@ %@", max, [symbolTable getValueWithKey:max]);
  NSLog(@"Access Time: %.2fms",
        1000.0f * (CFAbsoluteTimeGetCurrent() - accessStartTime));
  NSLog(@"Compares: %ld", (unsigned long)[symbolTable numberOfCompares]);
  NSLog(@"Total Time: %.2fms", 1000.0f * (CFAbsoluteTimeGetCurrent() - totalStartTime));
  NSLog(@"\n");
}

+ (NSArray *)inputWordsWithPath:(NSString *)path {
  NSString* inputString = [NSString stringWithContentsOfFile:path
                                                    encoding:NSUTF8StringEncoding
                                                       error:NULL];
  return [inputString componentsSeparatedByString:@" "];
}



@end
