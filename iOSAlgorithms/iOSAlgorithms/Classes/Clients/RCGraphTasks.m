#import "RCGraphTasks.h"

#import "RCGraph.h"
#import "RCBag.h"

@implementation RCGraphTasks

+ (NSInteger)degreeOfVertex:(NSInteger)vertex inGraph:(RCGraph *)graph {
  NSInteger degree = 0;
  NSEnumerator *enumerator = [[graph verticiesAdjacentToVertex:vertex] enumerator];
  for (id aVertex in enumerator) degree++;
  return degree;
}

+ (NSInteger)maxDegreeInGraph:(RCGraph *)graph {
  NSInteger max = 0;
  for (NSInteger v = 0; v < [graph numberOfVerticies]; v++) {
    NSInteger degree = [self degreeOfVertex:v inGraph:graph];
    if (degree > max) max = degree;
  }
  return max;
}

+ (double)averageDegreeInGraph:(RCGraph *)graph {
  return 2.0 * [graph numberOfEdges] / [graph numberOfVerticies];
}

+ (NSInteger)numberOfSelfLoopsInGraph:(RCGraph *)graph {
  NSInteger count = 0;
  for (NSInteger v = 0; v < [graph numberOfVerticies]; v++) {
    NSEnumerator *enumerator = [[graph verticiesAdjacentToVertex:v] enumerator];
    for (NSNumber *aVertex in enumerator) {
      if ([aVertex integerValue] == v) count ++;
    }
  }
  return count/2;
}

@end
