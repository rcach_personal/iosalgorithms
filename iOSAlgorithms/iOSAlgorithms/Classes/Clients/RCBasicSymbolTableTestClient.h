#import <Foundation/Foundation.h>

@interface RCBasicSymbolTableTestClient : NSObject

+ (void)test:(NSString *)inputString;

@end