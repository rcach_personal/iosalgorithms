#import "RCDegreesOfSeparationClient.h"

#import "RCBreadthFirstGraphPaths.h"
#import "RCGraph.h"
#import "RCSymbolGraph.h"

@implementation RCDegreesOfSeparationClient

- (void)run {
  NSString *filePath = [[NSBundle mainBundle] pathForResource:@"movies"
                                                       ofType:@"txt"];
  NSString *delimiter = @"/";
  RCSymbolGraph *symbolGraph = [RCSymbolGraph symbolGraphWithPath:filePath
                                                        delimiter:delimiter];
  RCGraph *graph = [symbolGraph graph];
  
  NSString *source = @"Animal House (1978)";
  NSString *target = @"Hanks, Tom";
  if (![symbolGraph containsKey:source]) {
    NSLog(@"Source: %@ not in database.", source);
    return;
  }
  if (![symbolGraph containsKey:target]) {
    NSLog(@"Target: %@ not in database.", target);
  }
  
  NSLog(@"%@", target);
  NSInteger sourceIndex = [symbolGraph indexForKey:source];
  NSInteger targetIndex = [symbolGraph indexForKey:target];
  RCBreadthFirstGraphPaths *paths =
      [RCBreadthFirstGraphPaths pathsWithGraph:graph sourceNode:sourceIndex];
  
  if ([paths hasPathToNode:targetIndex]) {
    for (NSNumber *node in [paths pathToNode:targetIndex]) {
      NSLog(@"    %@", [symbolGraph keyForIndex:[node integerValue]]);
    }
  } else {
    NSLog(@"Not Connected");
  }
}

@end
