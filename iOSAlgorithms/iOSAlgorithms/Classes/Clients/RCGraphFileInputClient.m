#import "RCGraphFileInputClient.h"

#import "RCGraph.h"

@implementation RCGraphFileInputClient

- (void)run {
  NSString *filepath = [[NSBundle mainBundle] pathForResource:@"tinyG" ofType:@"txt"];
  RCGraph *graph = [RCGraph graphFromInputFilePath:filepath];
  NSLog(@"%@", [graph description]);
}

@end
