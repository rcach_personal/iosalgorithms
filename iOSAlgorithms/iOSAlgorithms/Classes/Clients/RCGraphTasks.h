#import <Foundation/Foundation.h>

@class RCGraph;

@interface RCGraphTasks : NSObject

+ (NSInteger)degreeOfVertex:(NSInteger)vertex inGraph:(RCGraph *)graph;

+ (NSInteger)maxDegreeInGraph:(RCGraph *)graph;

+ (double)averageDegreeInGraph:(RCGraph *)graph;

+ (NSInteger)numberOfSelfLoopsInGraph:(RCGraph *)graph;

@end
